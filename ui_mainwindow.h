/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <include/qcustomplot.h>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QFrame *frame;
    QLineEdit *speedEdit;
    QLineEdit *elevationEdit;
    QLabel *label;
    QLabel *label_2;
    QPushButton *startButton;
    QPushButton *stopButton;
    QComboBox *comboBox;
    QLabel *label_3;
    QLabel *counter_label;
    QPushButton *heel_strike_p;
    QPushButton *toe_off_p;
    QLabel *label_8;
    QFrame *frame_2;
    QGridLayout *gridLayout;
    QCustomPlot *customPlot_2;
    QLabel *label_5;
    QCustomPlot *customPlot;
    QLabel *label_4;
    QCustomPlot *customPlot_4;
    QLabel *label_7;
    QCustomPlot *customPlot_3;
    QLabel *label_6;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(978, 700);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        frame = new QFrame(centralWidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(9, 508, 960, 161));
        frame->setMaximumSize(QSize(16777215, 11777215));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        speedEdit = new QLineEdit(frame);
        speedEdit->setObjectName(QString::fromUtf8("speedEdit"));
        speedEdit->setGeometry(QRect(10, 30, 113, 27));
        elevationEdit = new QLineEdit(frame);
        elevationEdit->setObjectName(QString::fromUtf8("elevationEdit"));
        elevationEdit->setGeometry(QRect(140, 30, 113, 27));
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 10, 67, 17));
        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(140, 10, 67, 17));
        startButton = new QPushButton(frame);
        startButton->setObjectName(QString::fromUtf8("startButton"));
        startButton->setGeometry(QRect(10, 70, 99, 27));
        stopButton = new QPushButton(frame);
        stopButton->setObjectName(QString::fromUtf8("stopButton"));
        stopButton->setGeometry(QRect(120, 70, 99, 27));
        comboBox = new QComboBox(frame);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setGeometry(QRect(270, 30, 85, 27));
        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(270, 10, 67, 17));
        counter_label = new QLabel(frame);
        counter_label->setObjectName(QString::fromUtf8("counter_label"));
        counter_label->setGeometry(QRect(280, 70, 67, 17));
        heel_strike_p = new QPushButton(frame);
        heel_strike_p->setObjectName(QString::fromUtf8("heel_strike_p"));
        heel_strike_p->setGeometry(QRect(820, 50, 99, 27));
        toe_off_p = new QPushButton(frame);
        toe_off_p->setObjectName(QString::fromUtf8("toe_off_p"));
        toe_off_p->setGeometry(QRect(820, 80, 99, 27));
        label_8 = new QLabel(frame);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(810, 20, 131, 17));
        speedEdit->raise();
        elevationEdit->raise();
        label->raise();
        label_2->raise();
        startButton->raise();
        stopButton->raise();
        comboBox->raise();
        label_3->raise();
        counter_label->raise();
        heel_strike_p->raise();
        toe_off_p->raise();
        label_8->raise();
        frame_2 = new QFrame(centralWidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setGeometry(QRect(9, 9, 961, 491));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame_2);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        customPlot_2 = new QCustomPlot(frame_2);
        customPlot_2->setObjectName(QString::fromUtf8("customPlot_2"));
        label_5 = new QLabel(customPlot_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(0, 0, 67, 17));
        QFont font;
        font.setFamily(QString::fromUtf8("Courier 10 Pitch"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label_5->setFont(font);

        gridLayout->addWidget(customPlot_2, 1, 0, 1, 1);

        customPlot = new QCustomPlot(frame_2);
        customPlot->setObjectName(QString::fromUtf8("customPlot"));
        label_4 = new QLabel(customPlot);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(0, 0, 67, 17));
        label_4->setFont(font);
        label->raise();
        label_4->raise();

        gridLayout->addWidget(customPlot, 0, 0, 1, 1);

        customPlot_4 = new QCustomPlot(frame_2);
        customPlot_4->setObjectName(QString::fromUtf8("customPlot_4"));
        label_7 = new QLabel(customPlot_4);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(0, 0, 67, 17));
        label_7->setFont(font);

        gridLayout->addWidget(customPlot_4, 1, 1, 1, 1);

        customPlot_3 = new QCustomPlot(frame_2);
        customPlot_3->setObjectName(QString::fromUtf8("customPlot_3"));
        label_6 = new QLabel(customPlot_3);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(0, 0, 67, 17));
        label_6->setFont(font);

        gridLayout->addWidget(customPlot_3, 0, 1, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "QCustomPlot plot examples", 0, QApplication::UnicodeUTF8));
        speedEdit->setText(QApplication::translate("MainWindow", "3.0", 0, QApplication::UnicodeUTF8));
        elevationEdit->setText(QApplication::translate("MainWindow", "0.0", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "Speed", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Elevation", 0, QApplication::UnicodeUTF8));
        startButton->setText(QApplication::translate("MainWindow", "Start", 0, QApplication::UnicodeUTF8));
        stopButton->setText(QApplication::translate("MainWindow", "Stop", 0, QApplication::UnicodeUTF8));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "TA", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "Sol", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "Per", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "VastLat", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "VastMed", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "RFem", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "Sar", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "Add", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "GlutMed", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "TFL", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "GastLat", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "GastMed", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "BFem", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "Semi", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "GlutMax", 0, QApplication::UnicodeUTF8)
        );
        label_3->setText(QApplication::translate("MainWindow", "Muscle", 0, QApplication::UnicodeUTF8));
        counter_label->setText(QApplication::translate("MainWindow", "TextLabel", 0, QApplication::UnicodeUTF8));
        heel_strike_p->setText(QApplication::translate("MainWindow", "Heel Strike", 0, QApplication::UnicodeUTF8));
        toe_off_p->setText(QApplication::translate("MainWindow", "Toe Off", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("MainWindow", "Sensor simulation", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "Sol", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "VastMed", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("MainWindow", "Semi", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("MainWindow", "TA", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
