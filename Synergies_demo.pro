SOURCES += \
    scr/main.cpp \
    scr/mainwindow.cpp \
    scr/MEP_class.cpp \
    scr/qcustomplot.cpp

HEADERS += \
    include/mainwindow.h \
    include/MEP_class.h \
    include/qcustomplot.h \
    include/ui_mainwindow.h

FORMS += \
    forms/mainwindow.ui
