#include <QApplication>
#include "include/mainwindow.h"
#include <locale.h>

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
  QApplication::setGraphicsSystem("raster");
#endif
  setlocale(LC_NUMERIC, "C");
  QApplication a(argc, argv);
  MainWindow w;
  //MainWindow w2;
  w.show();
  //w2.show();
  
  return a.exec();
}
